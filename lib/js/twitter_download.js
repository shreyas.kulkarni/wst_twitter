function callPDF() {
    $.ajax({
        type: 'get',
        url: 'pdf_download.php', //Here you will fetch records
        dataType: 'html',
        success: function (result) {
            var docDefinition = {content: result};
            pdfMake.createPdf(docDefinition).download();
        }
    });
}

$('#downloadAll').on('change', function () {
    var url = $(this).val();
    if (url) {
        if (url == 'pdf') {
            callPDF();
        }
        else {
            window.location = url;
        }
    }
});

$('.download_tweet').click(function () {
    $(".download_select").prop("disabled", true);
    $.ajax({
        type: 'get',
        url: 'store_tweets.php', 
        dataType: 'json',
        success: function (result) {
            if (result == 1) {
                $(".download_select").prop("disabled", false);
            }
            else {
                alert("Something went wrong while retriveing tweets!");
            }
        }
    });
});