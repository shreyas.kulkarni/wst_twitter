# WST Mini-Project

This is the wst mini-project based on the __rt-camp__ web-developer hiring [assignments](https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer).  
This is the Twitter Timeline Challenge while the Facebook Part of the Assignment has been implemented seperatly by [Atharva Jadhav](https://gitlab.com/atharvaa/).

Following is the list of goals implemented:
#### Part-1: User Timeline
1. Start => User visits the script page.
2. User will be asked to connect using his Twitter account using Twitter Auth.
3. After authentication, the scrip pulls latest 10 tweets from his "home" timeline.
4. 10 tweets are displayed as a slideshow.

#### Part-2: Followers Timeline
1. Below slideshow (in step#4 from part-1), list of 10 followers is displayed.
2. When user will click on a follower name, 10 tweets from that follower's user-timeline are displayed in the same slider, without page refresh (using AJAX).

#### Part-3: Download Followers
1. There is a download button to download all followers of a user.
2. Download can be performed in one of the following formats: PDF, CSV and JSON formats.
3. Once the user clicks download button and then chooses option all followers of the specified user are downloaded. 

## Installation

To run the application download the entire repo and navigate to the folder. Then run a php server using the following command 
```bash
php -S 127.0.0.1:3000
```

**Note** - Only use the _port 3000_ as it has been set on the Twitter developer site as the callback URL. To use another port the developer needs to make changes in the [App Settings](https://developer.twitter.com/en/apps) on Twitter and __ConsumerKey.php__ file.
## Usage

You can set up your own app on [Twitter Developer](http://developer.twitter.com) and then run the application with the **Callback URL** of your choice.  
If you choose to do so then you will also need to change the __ConsumerKey.php__ file. Set the *CONSUMER_KEY*, *CONSUMER_SECRET* and *OAUTH_CALLBACK* parameters with the new keys that you will get in your [Twitter App Settings](https://developer.twitter.com/en/apps).

## References and Libraries 
1. rtCamp - [https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer](https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer)
2. Twitter - [https://developer.twitter.com/en/docs](https://developer.twitter.com/en/docs)  
3. Abraham Williams Twitter oAuth - [https://github.com/abraham/twitteroauth](https://github.com/abraham/twitteroauth)
4. Bootstrap - [https://getbootstrap.com/docs/3.3/getting-started/](https://getbootstrap.com/docs/3.3/getting-started/)
5. makePDF.org - [js pdf generator](https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js)

## Submitted by
[Shreyas Kulkarni - 111608040](https://gitlab.com/shreyas.kulkarni)
[Atharva Jadhav - 111608031](https://gitlab.com/atharvaa/)