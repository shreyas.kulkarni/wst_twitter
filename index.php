<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Twitter App</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/style.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>

    <script src="lib/js/index.js"></script>
</head>
<body>
<?php
use Abraham\TwitterOAuth\TwitterOAuth;

require 'autoload.php';
require 'ConsumerKey.php';
if (!isset($_SESSION['access_token'])) {
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
    $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
    $_SESSION['oauth_token'] = $request_token['oauth_token'];
    $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
    $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
    ?>
    <div class="container">
        <div class="text-center">
            <h1 class="display-1">WST Miniproject</h1>
            <br>
            <img src="logo.png" class="img-thumbnail rounded logo" alt="Twitter_logo"></img>
            <h1>Twitter Timeline Challenge</h1>
            <h3>Click to login to twitter</h3>
            <br>
            <div class="login">
                <a href="<?php echo $url; ?>">
                    <button type="button" class="btn btn-success btn-lg btn-block" aria-pressed="true">Login</button>
                </a>
            </div>
        </div>
    </div>
    <?php
}
else {
    $access_token = $_SESSION['access_token'];
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
    $user = $connection->get("account/verify_credentials");
    $session_account_info = array('screen_name' => $user->screen_name, 'followers' => $user->followers_count);
    $_SESSION['my_profile'] = $session_account_info;
    ?>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">Twitter Timeline Challenge</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#myModal" class="download_tweet">
                            <span  class="glyphicon glyphicon-download-alt"></span> Download
                        </a>
                    </li>
                    <li>
                        <a href="logout.php">Logout <span class="glyphicon glyphicon-log-out"></span> </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="margin-top:20px">
        <div class="col-xs-12">
            <div id="slider">
                <a href="#" class="control_next">>></a>
                <a href="#" class="control_prev"><<</a>
                <ul id="sliderDiv">
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Download Tweets</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="sel1">Select option for download tweets:</label>
                            <select class="form-control download_select" id="downloadAll">
                                <option value="">Select</option>
                                <option value="csv_download.php">CSV</option>
                                <option value="json_download.php">Json</option>
                                <option value="pdf">Pdf</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="lib/js/twitter_js.js"></script>
    <br><br>
    <div class="container-fluid">
        <div class="col-md-9 col-md-offset-3 col-sm-12">
            <div class="col-md-12 col-sm-12">
                <?php
                $followerslist = $connection->get("followers/list", array('count' => 10));
                if (isset($followerslist->errors[0]->code)) {
                    echo "<div class='col-md-10 col-md-offset-2 col-sm-12'>Rate Limit exceeded please try after some time</div>";
                } else {
                    foreach ($followerslist->users as $follwers_random) {
                        echo "<div class='col-md-6  col-sm-12'  style='padding-bottom:40px;'>";
                        echo "<img src='$follwers_random->profile_image_url_https'>";
                        echo "<button class='btnfollowertwitt btn btn btn-lg' value='$follwers_random->screen_name'>$follwers_random->name</button>";
                        echo "</div>";
                    }
                }
            }
            ?>
            </div>
        </div>
    </div>
    <script src="lib/js/twitter_download.js"></script>
</body>
</html>